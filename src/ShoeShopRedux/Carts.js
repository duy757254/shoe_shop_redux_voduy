import { type } from "@testing-library/user-event/dist/type";
import React, { Component } from "react";
import { connect } from "react-redux";
import { UPDATE_QUANTITY } from "./redux/constant/shoeShopConstant";

class Carts extends Component {
  renderTbody = () => {
    return this.props.cartShop.map((item) => {
      return (
        <tr className="font-weight-bold border border-danger">
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img style={{ height: 70 }} src={item.image} alt="" />
          </td>
          <td>
            <i class="fa fa-dollar-sign"></i>
            {item.price * item.number}
          </td>
          <td className="text-center  ">
            <button
              onClick={() => {
                this.props.handleQuantity(item.id, +1);
              }}
              className="mr-2 btn btn-primary"
            >
              <i class="fa fa-plus"></i>
            </button>
            {item.number}
            <button
              onClick={() => {
                this.props.handleQuantity(item.id, -1);
              }}
              className="ml-2 btn btn-danger"
            >
              <i className="fa fa-minus" />
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr className=" font-weight-bold col-12">
            <td className="col-1">ID</td>
            <td className="col-4">NAME</td>
            <td>IMAGE</td>
            <td>
              <i class="fa fa-dollar-sign"></i>PRICE
            </td>
            <td>QUANTITY</td>
          </tr>
        </thead>

        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    cartShop: state.shoeReducers.cart,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleQuantity: (id, quantity) => {
      dispatch({
        type: UPDATE_QUANTITY,
        payload: {
          idShoe: id,
          quantityShoe: quantity,
        },
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Carts);
