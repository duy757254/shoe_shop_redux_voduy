import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_CART, VIEW_DETAIL } from "./redux/constant/shoeShopConstant";

class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.data;
    return (
      <div className="col-3 p-1  ">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h6 className="card-title"> {name} </h6>
            <p className="card-text">
              <i className="fa fa-dollar-sign" />

              {price}
            </p>
          </div>
          <div className="d-flex justify-content-center">
            <button
              onClick={() => {
                this.props.handleViewDetail(this.props.data);
              }}
              className=" btn btn-primary font-weight-bold w-100"
            >
              Xem chi tiết
            </button>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.data);
              }}
              className="btn btn-danger font-weight-bold w-100"
            >
              Mua
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (shoe) => {
      // let action = {
      //   type: ADD_CART,
      //   payload: shoe,
      // };
      dispatch({ type: ADD_CART, payload: shoe });
    },
    handleViewDetail: (shoe) => {
      dispatch({ type: VIEW_DETAIL, payload: shoe });
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemShoe);
