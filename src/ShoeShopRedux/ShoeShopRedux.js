import React, { Component } from "react";
import Carts from "./Carts";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class ShoeShopRedux extends Component {
  render() {
    return (
      <div className="container">
        <Carts />
        <ListShoe />
        <DetailShoe />
      </div>
    );
  }
}
