import { bindActionCreators } from "redux";
import { dataShoe } from "../../dataShoe";
import {
  ADD_CART,
  UPDATE_QUANTITY,
  VIEW_DETAIL,
} from "../constant/shoeShopConstant";

let intialState = {
  listShoe: dataShoe,
  cart: [],
  detail: dataShoe,
};

export const shoeReducers = (state = intialState, action) => {
  switch (action.type) {
    case ADD_CART: {
      let cloneCart = [...state.cart];

      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        let itemCart = { ...action.payload, number: 1 };
        cloneCart.push(itemCart);
      } else {
        if (cloneCart[index].number >= 10) {
          alert("Xin lỗi!! Không được mua quá 10 đôi");
          break;
        }
        cloneCart[index].number++;
      }
      // if (cloneCart[index].number >= 10) {
      //   alert("không được mua quá 10 đôi ");
      // }
      state.cart = cloneCart;
      return { ...state };
    }
    case UPDATE_QUANTITY: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.idShoe;
      });
      if (index >= 0) {
        cloneCart[index].number += action.payload.quantityShoe;
      }
      if (cloneCart[index].number == 0) {
        cloneCart.splice(index, 1);
      }
      return { ...state, cart: cloneCart };
    }
    case VIEW_DETAIL: {
      return { ...state, detail: action.payload };
    }
    default:
      return state;
  }
};
