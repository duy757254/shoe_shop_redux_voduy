import { combineReducers } from "redux";
import { shoeReducers } from "./shoeReducers";

export const rootReducer_shoeShop = combineReducers({ shoeReducers });
