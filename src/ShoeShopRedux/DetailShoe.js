import React, { Component } from "react";
import { connect } from "react-redux";

class DetailShoe extends Component {
  render() {
    let { image, name, description, price } = this.props.detail;
    return (
      <div className="text-left p-5 row  container mt-5  bg-dark ">
        <img className=" col-3" src={image} alt />

        <div className="pt-3 text-success   col-9">
          <h5>
            Tên Giày: <span className="text-light">{name}</span>
          </h5>
          <h5>
            Mô tả: <span className="text-primary">{description}</span>{" "}
          </h5>
          <p>
            Giá:{" "}
            <span className="text-danger">
              <i className="fa fa-dollar-sign" />

              {price}
            </span>{" "}
          </p>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    detail: state.shoeReducers.detail,
  };
};

export default connect(mapStateToProps)(DetailShoe);
