import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.ListShoe.map((item, index) => {
      return (
        <ItemShoe
          handleViewDetail={this.props.handleViewDetail}
          data={item}
          key={index}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}
let mapStateToProps = (state) => {
  return {
    ListShoe: state.shoeReducers.listShoe,
  };
};
export default connect(mapStateToProps)(ListShoe);
